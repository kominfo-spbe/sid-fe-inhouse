import axios from "axios";
import Swal from "sweetalert2";

const state = {
  status: "",
  token: localStorage.getItem("token") || "",
  user: {}
};

const getters = {
  isLoggedIn: state => !!state.token,
  authStatus: state => state.status,
  nameUserLogin: state => state.user
};

const actions = {
  login({ commit }, user) {
    let baseURL = process.env.VUE_APP_URL_LOCAL + "/auth/login";
    return new Promise((resolve, reject) => {
      commit("auth_request");
      axios({
        url: baseURL,
        data: user,
        method: "POST"
      })
        .then(resp => {
          console.log(resp);
          const token = resp.data.access_token;
          // const user = resp.data.username;
          localStorage.setItem("token", token);
          // Add the following line:
          axios.defaults.headers.common["Authorization"] = "Bearer ${token}";
          commit("auth_success", token, user);
          resolve(resp);
        })
        .catch(err => {
          console.log(err.response, "errorrrrr");
          Swal.fire({
            title: "",
            text: err.response.data.message,
            icon: "error",
            heightAuto: false
          }).then(err => {
            commit("auth_error");
            localStorage.removeItem("token");
            reject(err);
          });
          /*commit("auth_error");
          localStorage.removeItem("token");
          reject(err);*/
        });
    });
  },
  logout({ commit }) {
    return new Promise(resolve => {
      commit("logout");
      localStorage.removeItem("token");
      delete axios.defaults.headers.common["Authorization"];
      resolve();
    });
  }
};

const mutations = {
  auth_request(state) {
    state.status = "loading";
  },
  auth_success(state, token, user) {
    state.status = "success";
    state.token = token;
    state.user = user;
  },
  auth_error(state) {
    state.status = "error";
  },
  logout(state) {
    state.status = "";
    state.token = "";
    state.user = "";
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};
