# Check out https://hub.docker.com/_/node to select a new base image
#FROM registry.spbe.sangkuriang.co.id/node:14.17.5-slim
FROM node:14-slim as build-stage

# Define default ARGS
ARG VUE_APP_URL_LOCAL="SECRET"
ARG VUE_APP_FILE_DOWNLOAD_URL="SECRET"
ARG VUE_APP_FILE_DOWNLOAD_PATH="SECRET"
ARG VUE_APP_SSO_KOMINFO_URL="SECRET"
ARG VUE_APP_SSO_KOMINFO_REALM="SECRET"
ARG VUE_APP_SSO_KOMINFO_CLIENT_ID="SECRET"
ARG VUE_APP_SSO_KOMINFO_CLIENT_SECRET="SECRET"
ARG VUE_APP_SSO_KOMINFO_CALLBACK="SECRET"

ARG VUE_APP_SSO_POLPUM_URL="SECRET"
ARG VUE_APP_SSO_POLPUM_CALLBACK="SECRET"
ARG VUE_APP_RECAPTCHA_V3_SITE_KEY="SECRET"

WORKDIR /app

COPY package*.json ./

RUN npm install
#RUN npm ci

COPY . .
#COPY env.dev .env
RUN npm run build

FROM bitnami/nginx:latest as production-stage
# RUN mkdir /app
COPY --from=build-stage /app/dist /app
COPY nginx.conf /opt/bitnami/nginx/conf/nginx.conf

EXPOSE 3000